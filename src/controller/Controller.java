package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static double getMin(IntegersBag bag)
	{
		return model.getMin(bag);
	}
	
	public static String getEvenNumbers (IntegersBag bag)
	{
		String answer = "";
		if (model.getEvenNumbers(bag).size() != 0)
		{
			answer += "The even numbers in the bag are: \n";
			for (int even: model.getEvenNumbers(bag))
				answer += even + "\n";
		}
		else
			answer = "There were no even numbers in the bag.";
		return answer;
	}
	
	public static String getOddNumbers (IntegersBag bag)
	{
		String answer = "";
		if (model.getOddNumbers(bag).size() != 0)
		{
			answer += "The odd numbers in the bag are: \n";
			for (int even: model.getOddNumbers(bag))
				answer += even + "\n";
		}
		else
			answer = "There were no odd numbers in the bag.";
		return answer;
	}
	
	public static int getNumberFrecuency(IntegersBag bag, int number)
	{
		return model.getNumberFrequency(bag, number);
	}
}
