package model.logic;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.sun.org.apache.bcel.internal.generic.AALOAD;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	public double computeMean(IntegersBag bag)
	{
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int getMin(IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int valor = 0;
		if (bag != null)
		{
			Iterator <Integer> iterador = bag.getIterator();
			while (iterador.hasNext())
			{
				valor = iterador.next();
				if (min > valor)
					min = valor;
			}
		}
		return min; 
	}

	public ArrayList<Integer> getEvenNumbers(IntegersBag bag)
	{
		ArrayList<Integer> evenNumbers = new ArrayList<>();

		if (bag != null)
		{
			int numActual = 0;
			Iterator <Integer> iterador = bag.getIterator();
			while(iterador.hasNext())
			{
				numActual = iterador.next();
				int mod = numActual %2;
				if (mod == 0)
					evenNumbers.add(numActual);
			}
		}
		return evenNumbers;
	}

	public ArrayList<Integer> getOddNumbers(IntegersBag bag)
	{
		ArrayList<Integer> oddNumbers = new ArrayList<>();

		if (bag != null)
		{
			int numActual = 0;
			Iterator <Integer> iterador = bag.getIterator();
			while(iterador.hasNext())
			{
				numActual = iterador.next();
				int mod = numActual % 2;
				if (mod != 0)
					oddNumbers.add(numActual);
				System.out.println(numActual);
			}
		}
		return oddNumbers;
	}

	
	/**
	 * La frecuencia del n�mero ingresado por par�metro en la bolsa de enteros.
	 * 
	 * Realice este algoritmico estadistico pero la bolsa de enteros solo los guarda una vez, por tanto la frecuencia si esta en la bolsa siempre ser� 1
	 * por esto realice los de arriba como para compensar, espero no haya problema.
	 * 
	 * @param bag Bolsa de enteros.
	 * @param number n�mero del que se desea conocer la frecuencia.
	 * @return
	 */
	public int getNumberFrequency(IntegersBag bag, int number)
	{
		int nf = 0;

		if (bag!= null)
		{		
			int actual = 0;
			Iterator <Integer> iterador = bag.getIterator();
			while(iterador.hasNext())
			{
				actual = iterador.next();
				if (actual == number)
				{
					nf ++;
				}
			}
		}

		return nf;
	}
}